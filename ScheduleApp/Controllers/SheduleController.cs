﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ScheduleApp.Controllers
{
    public class SheduleController : Controller
    {
        private EFContext _context;

        public SheduleController(EFContext context)
        {
            _context = context;
        }

        public IActionResult Index(int? groupId)
        {
            int selectgroupId = GroupValid(groupId);

            var today = DateTime.Today;
            today = today.DayOfWeek == DayOfWeek.Sunday ? today.AddDays(1) : today;
            var startDay = today.AddDays((today.DayOfWeek >= DayOfWeek.Monday && today.DayOfWeek <= DayOfWeek.Tuesday) ? -3 : -2);
            var endDay = today.AddDays((today.DayOfWeek >= DayOfWeek.Friday && today.DayOfWeek <= DayOfWeek.Saturday) ? 3 : 2);

            var shedules = _context.Shedules.Where(s => s.GroupId == selectgroupId && s.Date >= today.AddDays(-2) && s.Date <= today.AddDays(+2));

            var result = shedules
            .Select(s => new Models.SheduleViewModel
            {
                Date = s.Date,
                Shedules = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, Models.PairViewModel>>(s.Shedules)
            }).ToList();

            for (DateTime day = startDay; day <= endDay; day = day.AddDays(1))
            {
                if (day.DayOfWeek == DayOfWeek.Sunday)
                {
                    continue;
                }
                if (result.Find(s => s.Date == day) == null)
                {
                    result.Add(new Models.SheduleViewModel
                    {
                        Date = day,
                        Shedules = new Dictionary<int, Models.PairViewModel>
                    {
                        { 0, new Models.PairViewModel { Name = "", Cabinet = "" } },
                        { 1, new Models.PairViewModel { Name = "", Cabinet = "" } },
                        { 2, new Models.PairViewModel { Name = "", Cabinet = "" } },
                        { 3, new Models.PairViewModel { Name = "", Cabinet = "" } },
                        { 4, new Models.PairViewModel { Name = "", Cabinet = "" } },
                        { 5, new Models.PairViewModel { Name = "", Cabinet = "" } }
                    }
                    });
                }
            }

            var groups = _context.Groups.Where(g => g.StartDate <= today && g.EndDate >= today).ToList();
            ViewData["GroupId"] = new SelectList(groups, "Id", "Name", groups.FirstOrDefault(g => g.Id == selectgroupId)?.Id);

            return View(result.OrderBy(s => s.Date));
        }

        public IActionResult EditGroup(int? groupId)
        {
            var groups = _context.Groups.ToList();
            groups.Add(new Models.Group
            {
                Id = 0,
                Name = "Новая группа"
            });

            if (groupId != null)
            {
                var group = groups.FirstOrDefault(g => g.Id == groupId.Value);
                if (group != null)
                {
                    ViewData["Id"] = new SelectList(groups.OrderBy(g => g.Id), "Id", "Name", group.Id);
                    return View(new Models.GroupEditModel
                    {
                        Id = group.Id,
                        Name = group.Name,
                        StartDate = group.StartDate,
                        EndDate = group.EndDate
                    });
                }      
            }
            ViewData["Id"] = new SelectList(groups.OrderBy(g => g.Id), "Id", "Name", 0);

            var model = new Models.GroupEditModel
            {
                StartDate = DateTime.Today,
                EndDate = DateTime.Today
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult EditGroup(int? groupId, Models.GroupEditModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.StartDate < model.EndDate)
                {
                    if (model.Id == 0)
                    {
                        if (_context.Groups.FirstOrDefault(g => g.Name == model.Name) == null)
                        {
                            _context.Groups.Add(new Models.Group
                            {
                                Name = model.Name,
                                StartDate = model.StartDate,
                                EndDate = model.EndDate
                            });
                            _context.SaveChanges();
                            ViewData["Message"] = "Группа успешно добавленна";
                        }
                        else
                        {
                            ViewData["ErrorMessage"] = "Группа с данным именем уже существует";
                        }
                    }
                    else
                    {
                        var group = _context.Groups.Find(model.Id);
                        if ((group.Name != model.Name && _context.Groups.FirstOrDefault(g => g.Name == model.Name) == null) || group.Name == model.Name)
                        {                           
                            group.Name = model.Name;
                            group.StartDate = model.StartDate;
                            group.EndDate = model.EndDate;
                            _context.SaveChanges();
                            ViewData["Message"] = "Группа успешно обновлена";
                        }
                        else
                        {
                            ViewData["ErrorMessage"] = "Группа с данным именем уже используется";
                        }
                    }      
                }
                else
                {
                    ViewData["ErrorMessage"] = "Дата окончания должна быть больше даты поступления";
                } 
            }
            var groups = _context.Groups.ToList();
            groups.Add(new Models.Group
            {
                Id = 0,
                Name = "Новая группа"
            });

            ViewData["Id"] = new SelectList(groups.OrderBy(g => g.Id), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public IActionResult DeleteGroup(Models.GroupEditModel model)
        {
            if (model.Id != 0)
            {
                var group = _context.Groups.FirstOrDefault(g => g.Id == model.Id);
                if (group != null)
                {
                    _context.Groups.Remove(group);
                    _context.SaveChanges();
                    TempData["Message"] = "Группа успешно удалена";
                }
                else
                {
                    TempData["ErrorMessage"] = "Группа не существует";
                }
            }
            return Redirect("EditGroup");
        }

        public IActionResult EditShedule(int? groupId, DateTime? date)
        {
            var today = DateTime.Today;

            var selectgroupId = GroupValid(groupId);

            var groups = _context.Groups.Where(g => g.StartDate <= today && g.EndDate >= today).ToList();

            var selectdate = date != null ? date.Value : today;
            var selectgroup = groups.FirstOrDefault(g => g.Id == selectgroupId);

            ViewData["GroupId"] = new SelectList(groups, "Id", "Name", selectgroup?.Id);

            Dictionary<int, Models.PairViewModel> shedules = new Dictionary<int, Models.PairViewModel>
                    {
                        { 0, new Models.PairViewModel { Name = "", Cabinet = ""} },
                        { 1, new Models.PairViewModel { Name = "", Cabinet = ""} },
                        { 2, new Models.PairViewModel { Name = "", Cabinet = ""} },
                        { 3, new Models.PairViewModel { Name = "", Cabinet = ""} },
                        { 4, new Models.PairViewModel { Name = "", Cabinet = ""} },
                        { 5, new Models.PairViewModel { Name = "", Cabinet = ""} }
                    };

            if (selectgroup != null)
            {
                var tmpshed = _context.Shedules.FirstOrDefault(s => s.GroupId == selectgroup.Id && s.Date == selectdate);

                if (tmpshed != null)
                {
                    shedules = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, Models.PairViewModel>>(
                        tmpshed.Shedules
                    );
                }
            }

            var model = new Models.ShedulesEditModel()
            {
                Date = today,
                Shedules = shedules
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult EditShedule(int? groupId, Models.ShedulesEditModel model) {
            var today = DateTime.Today;
            var groups = _context.Groups.Where(g => g.StartDate <= today && g.EndDate >= today).ToList();
            if (groupId != null)
            {
                ViewData["GroupId"] = new SelectList(groups, "Id", "Name", groups.FirstOrDefault(g => g.Id == groupId));
            }
            else
            {
                ViewData["GroupId"] = new SelectList(groups, "Id", "Name");
            }

            if (ModelState.IsValid) {
                var shed = _context.Shedules.FirstOrDefault(s => s.GroupId == model.GroupId && s.Date == model.Date);

                if (shed != null)
                {
                    shed.Shedules = Newtonsoft.Json.JsonConvert.SerializeObject(model.Shedules);

                    _context.Shedules.Update(shed);
                    _context.SaveChanges();
                }
                else
                {
                    _context.Shedules.Add(new Models.Shedule()
                    {
                        GroupId = model.GroupId,
                        Date = model.Date,
                        Shedules = Newtonsoft.Json.JsonConvert.SerializeObject(model.Shedules)
                    });
                }

                _context.SaveChanges();

                ViewData["Message"] = "Расписание успешно сохранено!";
            }

            return View(model);
        }


        private int GroupValid(int? groupId) {
            int result = 0;

            if (groupId != null)
            {
                var group = _context.Groups.FirstOrDefault(g => g.Id == groupId.Value);
                if (group != null)
                {
                    result = group.Id;
                }
                else
                {
                    group = _context.Groups.FirstOrDefault();
                    result = group != null ? group.Id : 0;
                }
            }
            else
            {
                var group = _context.Groups.FirstOrDefault();
                result = group != null ? group.Id : 0;
            }

            return result;
        }
    }
}