﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql;
using ScheduleApp.Models;

namespace ScheduleApp
{   
    public class EFContext : DbContext
    {
        public DbSet<Models.Group> Groups { get; set; }
        public DbSet<Models.Shedule> Shedules { get; set; }

        public EFContext(DbContextOptions options) 
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
