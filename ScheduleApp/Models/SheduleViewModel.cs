﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleApp.Models
{
    public class SheduleViewModel
    {
        public DateTime Date { get; set; }
        public Dictionary<int, PairViewModel> Shedules { get; set; }
    }
    public class PairViewModel
    {
        public string Name { get; set; }
        public string Cabinet { get; set; }
    }
}

