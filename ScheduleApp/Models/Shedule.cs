﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ScheduleApp.Models
{
    public class Shedule
    {
        public int Id { get; set; }
        [Required, ForeignKey("Group")]
        public int GroupId { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public string Shedules { get; set; }

        public virtual Group Group { get; set; }
    }
}
