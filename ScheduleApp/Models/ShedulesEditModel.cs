﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScheduleApp.Models
{
    public class ShedulesEditModel
    {
        [Display(Name = "Группа:"), Required(ErrorMessage = "Группа обязательна к заполнению")]
        public int GroupId { get; set; }

        [Display(Name = "Дата:"), Required(ErrorMessage = "Дата обязательна к заполнению")]

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = false)]
        public DateTime Date { get; set; }

        public Dictionary<int, PairViewModel> Shedules { get; set; }
    }
}

