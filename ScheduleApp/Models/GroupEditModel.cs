﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScheduleApp.Models
{
    public class GroupEditModel
    {
        public int Id { get; set; }

        [Display(Name = "Нзвание группы:"), Required(ErrorMessage = "Название группы обезательно для заполнения"), StringLength(32, MinimumLength = 4, ErrorMessage = "Название группы должно быть больше 4-х символов и не более 32-х")]
        public string Name { get; set; }

        [Display(Name = "Дата поступления:"), Required(ErrorMessage = "Дата поступления обезателен для заполнения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = false)]
        public DateTime StartDate { get; set; }

        [Display(Name = "Дата окончания:"), Required(ErrorMessage = "Дата окончания обучения обезателен для заполнения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = false)]
        public DateTime EndDate { get; set; }
    }
}
